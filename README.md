# About Requirements Engineering

See [Requirements Engineering on Wikipedia](https://en.wikipedia.org/wiki/Requirements_engineering)

* [Understand Product Design](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/understand-product-design)
* [Defining Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/defining-requirements)
* [Documenting Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements)
* [Maintaining Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/maintaining-requirements)
